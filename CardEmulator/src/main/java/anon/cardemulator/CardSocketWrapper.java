package anon.cardemulator;

import android.os.AsyncTask;
import android.util.Log;

import anon.nfccomm.SocketWrapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java9.util.function.Consumer;

/**
 * A specific socket wrapper for use by the card emulator.
 */
public class CardSocketWrapper extends SocketWrapper {
    // The function called with data to be sent to the terminal.
    private final Consumer<MyByteArray> sendToTerminal;
    // A queue to store data received from the terminal.
    public BlockingQueue<MyByteArray> receivedFromTerminal = new LinkedBlockingQueue<>();
    private String new_nfa = "NFA_DM_START_UP_CFG={4B:CB:01:01:A5:01:01:CA:17:00:00:00:00:06:00:" +
            "00:00:00:0F:00:00:00:00:E0:67:35:00:14:01:00:00:10:B5:03:01:02:FF:80:01:01:C9:03:03:" +
            "0F:AB:5B:01:00:B2:04:E8:03:00:00:CF:02:02:08:B1:06:00:20:00:00:00:12:C2:02:00:C8:33:04:";

    CardSocketWrapper(ServerSocket serverSocket, Consumer<String> statusFunc, Consumer<MyByteArray> sendToTerminal) {
        // The no-op lambda is the function to be run when the socket is closed (nothing required in
        // this case).
        super(serverSocket, statusFunc, () -> {

        });

        this.sendToTerminal = sendToTerminal;
    }

    @Override
    protected synchronized void waitForConn() {
        super.waitForConn();
        statusFunc.accept("Remote connection made. Ready for communication.");
    }

    /**
     * Send data to the server, and return its reply.
     * @param data: The data to be sent to the server.
     * @return: The reply received from the server.
     */
    private byte[] serverTransceive(byte[] data) {
        try {
            send(data);
            return receive();
        } catch (IOException e) {
            Log.e(TAG, "There was an error while attempting to "
                    + " communicate with the server:");
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    /**
     * Add a command that has been received from the terminal to the queue, to be sent to the
     * server.
     * @param data: The command received.
     */
    void processTerminalCmd(byte[] data) {
        Log.d(TAG,"terminalCmd to be processed");
        receivedFromTerminal.add(new MyByteArray(data));
    }

    void setUID(String newUid) {
        try{

            Log.d(TAG, "Attempting to remount /");
            Process su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(su.getInputStream()));

            outputStream.writeBytes("mount -o rw,remount /system\n");
            outputStream.flush();

            String config_id = Utils.insertPeriodically(newUid, ":", 2);
            Log.d(TAG, "new format id: " + config_id);
            outputStream.writeBytes("sed -i \"s/^NFA_DM_START_UP_CFG=.*/" + new_nfa +
                    config_id + "}/g\" /etc/libnfc-brcm-20791b05.conf\n");
            outputStream.flush();

            outputStream.writeBytes("mount -o ro,remount /system\n");
            outputStream.flush();

//          Restart NFC:
            outputStream.writeBytes("service call nfc 7\n");
            outputStream.flush();
//
            Thread.sleep(10);
            outputStream.writeBytes("service call nfc 8\n");
            outputStream.flush();

            outputStream.writeBytes("exit\n");
            outputStream.flush();


            su.waitFor();

            Log.d(TAG, "Should be done.");

            su.destroy();
        }catch(IOException e) {
            Log.e(TAG, e.getMessage());
        }catch(SecurityException e){
            Log.e(TAG, e.getMessage());
        }catch(InterruptedException e){
            Log.e(TAG, e.getMessage());
        }

    }

    /**
     * The main task to be run in the background, transceiving commands and responses.
     */
    void run() {

        class CardAsyncTask extends AsyncTask<Void, Void, Void> {
            @Override
            public Void doInBackground(Void... params) {
                Log.e(TAG,"DO IN BACKGROUND");
                String newUid;
                newUid = "AABBCCFF";

                while(true) {
                    try {
                        byte[] fromTermEmu = receive();
                        if (fromTermEmu != null) {
                            newUid = Utils.byteArrayToHexString(fromTermEmu);
                            Log.d(TAG, "Received msg with new id: " + newUid);
                            break;
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "There was an error while attempting to "
                                + " communicate with the server:");
                        Log.e(TAG, e.getMessage());
                        return null;
                    }
                }

//                // Set UID
                Log.e(TAG,"Trying to set UID");
                setUID(newUid);


                try {

                    while (true) {
                        byte[] command = receivedFromTerminal.take().data;

                        if (command == null) {
                            Log.i(TAG, "Received empty command - clearing.");
                            receivedFromTerminal.clear();
                            //// Break out of receiving commands for this connection.
                            //break;
                        }
                        Log.d(TAG, "Received command of length "
                                + command.length);
                        byte[] response = serverTransceive(command);
                        if (response == null) {
                            Log.i(TAG, "Received null from server. Attempting to restart connection.");
                            close();
                            open();
                            continue;
                        }
                        sendToTerminal.accept(new MyByteArray(response));
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                }
                return null;
            }
        }
        new CardAsyncTask().execute();
    }

    /**
     * Erase all messages received from the terminal from the queue.
     */
    public void clearReceivedFromTerminal() {
        receivedFromTerminal.clear();
    }
}


