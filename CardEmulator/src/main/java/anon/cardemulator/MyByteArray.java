package anon.cardemulator;

/**
 * A barebones data class to wrap a bytearray and allow it to be used with generics, without having
 * to convert between Byte[] and byte[].
 */
public class MyByteArray {
    public byte[] data;
    
    public MyByteArray(byte[] data) {
        this.data = data;
    }
}
