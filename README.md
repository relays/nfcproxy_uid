# NFCProxy_uid

This repo hosts our modified Android Apps for performing the Visa relay
attack (based on [NFCProxy](https://gitlab.com/relays/nfcproxy)).

## Usage

- modified relay for Visa relay attack
- requires the relay server `NFCProxy_uid_relay.py` (find it [here](https://gitlab.com/relays/server/-/blob/main/NFCProxy_uid_relay.py))
- similar to `NFCProxy`
- it will read the card UID with the `TerminalEmulator` app, and send it to the
  `CardEmulator` via the relay server. `CardEmulator` will set the device UID
  to the received value
- `CardEmulator` is custom-tailored for a Nexus 5 (hammerhead) phone, running 
  Android 7.1

```sh
usage: NFCProxy_uid_relay.py [-h] [-tp TERMPORT] [-cp CARDPORT] TermEmulatorIP CardEmulatorIP

Relay server connecting to a terminal emulator and a card emulator

positional arguments:
  TermEmulatorIP        the IP of the Terminal Emulator APP
  CardEmulatorIP        the IP of the Card Emulator APP

optional arguments:
  -h, --help            show this help message and exit
  -tp TERMPORT, --termPort TERMPORT
                        the port for the Terminal Emulator APP (default: 59557)
  -cp CARDPORT, --cardPort CARDPORT
                        the port for the Card Emulator APP (default: 59556)

```